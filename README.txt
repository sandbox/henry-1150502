// $Id: README.txt,v 1.0 2011/05/30 $

ANONYMOUS NODE MODULE
-----------------

Currently maintained by: Henry Hu (http://mynoteweb.com)

This module allows nodes that are created by an anonymous user to be handled by the authenticated user who logged in from a same machine. The connection is identified and built automatically.

When an anonymous user who has created some nodes logs in, the module will identify the anonymous user with the authenticated user, and automatically created duplicated nodes for the latter. Every change on those nodes made by either the anonymous or authenticated user will be applied to the corresponding nodes.

The functionality provides the convenience that users can perform orperations without logging in, and be free from the risk of exposing all nodes by *remember me* machanism.

You can check it out at http://mynoteweb.com.

Please report any bugs, feature requests, etc. at: http://drupal.org/sandbox/henry/1150502


Installation
------------
1. Copy anonymous_node folder to your sites/all/modules directory.
2. At Administer -> Site building -> Modules (admin/build/modules) enable the module.

Note that you should grante anonymous users to create their own nodes, or you will not get any effect.


